#!/bin/bash

gpg --keyserver keyring.debian.org --recv-key DF9B9C49EAA9298432589D76DA87E80D6294BE9B 

URL="https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/"
FILES="SHA512SUMS SHA512SUMS.sign debian-live-11.2.0-amd64-gnome.iso"

for i in $FILES
do
echo $i
wget -c $URL/$i
done

sha512sum -c SHA512SUMS
gpg --verify SHA512SUMS.sign SHA512SUMS
