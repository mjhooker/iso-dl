#!/bin/bash

#gpg --keyserver keyring.debian.org --recv-key DF9B9C49EAA9298432589D76DA87E80D6294BE9B 

CHECKSUMPRG="sha256sum -c"

URL="https://frafiles.pfsense.org/mirror/downloads"
FILES="pfSense-CE-2.4.4-RELEASE-p3-amd64.iso.gz"

for i in $FILES
do
echo $i
wget -c $URL/$i
done

URL="https://files.pfsense.org/hashes"
FILES="$FILES.sha256"

for i in $FILES
do
echo $i
wget -c $URL/$i
done


${CHECKSUMPRG} $FILES
#gpg --verify SHA512SUMS.sign SHA512SUMS
